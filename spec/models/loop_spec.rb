require 'rails_helper'

RSpec.describe Loop, type: :model do
  describe '#hashes' do
    let (:loop) { Loop.new }
    let (:standard_number_of_hashes) { 3 }
    subject { loop.hashes.count }

    context 'no number set' do
      let (:loop) { Loop.new }

      it { is_expected.to eql standard_number_of_hashes }
    end

    context 'number_of_hashes_set' do
      let (:loop) { Loop.new(number_of_hashes) }
      let (:number_of_hashes) { 5 }

      it { is_expected.to eql number_of_hashes }
    end

    context 'uniqueness' do
      subject { loop.hashes.uniq.count }

      it { is_expected.to eql standard_number_of_hashes }
    end
  end

  describe '#name' do
    let(:loop) { Loop.new }
    let(:standard_number) { 3 }
    subject { loop.loop_name }

    context 'standard name' do
      let(:standard_name) { 'Test1' }

      context 'standard constructor' do
        it { is_expected.to eql standard_name }
      end

      context 'number of hashes set' do
        let(:loop) { Loop.new(standard_number) }

        it { is_expected.to eql standard_name }
      end
    end

    context 'name set' do
      let(:loop) { Loop.new(standard_number, name) }
      let(:name) { 'my loop' }

      it { is_expected.to eql name }
    end
  end
end
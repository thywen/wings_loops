module ApplicationHelper
  def self.maximum_of_loops
    10
  end

  def self.maximum_inner_loop
    3
  end

  def self.get_inner_loop
    [*1..maximum_inner_loop]
  end

  def self.get_loops_for_run
    [*1..maximum_of_loops]
  end
end

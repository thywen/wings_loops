class MultiLoop
  attr_reader :hash, :inner_hashes, :loop_name

  def initialize(number_of_hashes, loop_name, number_of_inner_loops, name_of_inner_loop)
    @loop_name = loop_name
    @hash = create_random_hashes(number_of_hashes.to_i)
    @inner_hashes = {}
    prepare_inner_loop(number_of_inner_loops.to_i, name_of_inner_loop)
  end

  def prepare_inner_loop(number_of_inner_loops, name_of_inner_loop)
    @hash.each do | hash |
      @inner_hashes[hash.to_sym] = Loop.new(number_of_inner_loops, name_of_inner_loop)
    end
  end

  def create_random_hashes(number_of_hashes)
    Array.new(number_of_hashes) { create_random_hash }
  end

  def create_random_hash
    Digest::MD5.hexdigest(Random.rand(100).to_s)
  end
end
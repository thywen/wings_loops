class Loop
  include ActiveModel::Model

  attr_reader :loop_name, :hashes

  def initialize(number_of_hashes = 3, loop_name = "Tes t1")
    @loop_name = loop_name
    @hashes = create_random_hashes(number_of_hashes)
  end

  def create_random_hashes(number_of_hashes)
    Array.new(number_of_hashes) { create_random_hash }
  end

  def create_random_hash
    Digest::MD5.hexdigest(Random.rand(100).to_s)
  end
end
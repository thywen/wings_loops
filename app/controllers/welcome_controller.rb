class WelcomeController < ApplicationController
  def index
    @loops_for_select = ApplicationHelper.get_loops_for_run
    @inner_loops = ApplicationHelper.get_inner_loop
  end
end
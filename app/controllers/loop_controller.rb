class LoopController < ApplicationController
  def index
    @loop = Loop.new(params[:number_of_loops].to_i)
    @name = params[:test_run_name]
    @hashes = @loop.hashes
    @loops_for_select = ApplicationHelper.get_loops_for_run
  end
end
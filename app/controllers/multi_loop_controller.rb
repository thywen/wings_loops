class MultiLoopController < ApplicationController
  def index
    @name = params[:test_run_name]
    @multi_loop =
        MultiLoop.new(
          params[:outer_loops],
          params[:outer_loop_name],
          params[:inner_loops],
          params[:inner_loop_name]
    )
  end
end